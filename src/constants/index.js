//NOTE: SCREEN TITLE 🍎🍎🍎🍎
export const screenTitle = {
    LOGIN: 'Login',
    USERS: 'User_Selection',
    NFT: 'NFT'
}

//NOTE: FONT NAME
export const fontsName = {
    FONT_BLACK: 'Inter-Black',
    FONT_BOLD: 'Inter-Bold',
    FONT_EXTRA_BOLD: 'Inter-ExtraBold',
    FONT_REGULAR: 'Inter-Regular'
}


//NOTE: API END POINTS 🍎🍎🍎🍎
export const USERS_API = 'https://jsonplaceholder.typicode.com/users';

//NOTE: ACTION TITLE 🍎🍎🍎🍎
export const actionTitle = {
	SELECTED_USERS: 'Selected Users',
}