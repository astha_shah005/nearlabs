import * as TextStyles from './textStyle';
import * as ViewStyles from './viewStyle';
import * as ButtonStyles from './buttonStyle';
import * as ImageStyle from './imageStyle';

module.exports = {
  ...TextStyles,
  ...ViewStyles,
  ...ButtonStyles,
  ...ImageStyle,
};