const LOGO_APP = require('./logo_black_NEAR.png');
const LOGIN_DESIGN = require('./login_Design.png');
const RIGHT_ARROW = require('./rightArrow.png');
const CLOSE = require('./close.png');
const I_ICON = require('./i_icon.png');
const NEAR_VECTOR = require('./near_vector.png');
const LOGO_NFTS = require('./LOGO_NFTS.png');

const AppImages = {
    LOGO_APP,
    LOGIN_DESIGN,
    RIGHT_ARROW,
    CLOSE,
    I_ICON,
    NEAR_VECTOR,
    LOGO_NFTS
  };
  export default AppImages;